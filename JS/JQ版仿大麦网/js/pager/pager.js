;
(function () {

    window.pager = function (container, pageCount, jump) {

        $(container).append(`
                        <ul class="pager">
                                <li class="ctrl first disabled">首页</li>
                                <li class="ctrl prev disabled">上一页</li>
                                <li class="ctrl next">下一页</li>
                                <li class="ctrl last">末页</li>
                        </ul>`)

        var pageIndex = 1

        for (let i = 1; i <= pageCount; i++) {
            $(".pager li").eq(i).after(`<li class="page-item">${i}</li>`)
        }
        $(".page-item").eq(0).addClass("active")
        // $(".pager").html(template("demo", new Array(pageCount)))

        $(".page-item").click(function () {
            pageIndex = parseInt($(this).text())
            $(this).addClass("active").siblings(".active").removeClass("active")
            check()
        })
        $(".next").click(function () {
            // $(".page-item.active").removeClass("active").next().addClass('active')
            if (pageCount === pageIndex) return false
            $(".page-item").eq(++pageIndex - 1).addClass("active").siblings(".active").removeClass(
                "active")
            check()
        })
        $(".prev").click(function () {
            if (pageIndex === 1) return false
            $(".page-item").eq(--pageIndex - 1).addClass("active").siblings(".active").removeClass(
                "active")
            check()
        })

        $(".first").click(function () {
            pageIndex = 1
            $(".page-item").eq(pageIndex - 1).addClass("active").siblings(".active").removeClass(
                "active")
            check()
        })

        $(".last").click(function () {
            pageIndex = pageCount
            $(".page-item").eq(pageIndex - 1).addClass("active").siblings(".active").removeClass(
                "active")
            check()
        })


        function check() {
            $(".first,.prev,.last,.next").removeClass("disabled")
            if (pageIndex === 1) {
                $(".first,.prev").addClass("disabled")
            } else if (pageIndex === pageCount) {
                $(".last,.next").addClass("disabled")
            }

            jump(pageIndex)
        }
    }
})();