var s = {
    setCookie(key, val, minute) {
        var now = new Date()
        now.setMinutes(now.getMinutes() + minute)
        document.cookie = `${key}=${val};expires=${now
                .toUTCString()}`
    },
    getCookie(key) {
        var cookies = document.cookie.split('; ')
        for (let i = 0; i < cookies.length; i++) {
            const item = cookies[i];
            var cookiearr = item.split('=')
            if (cookiearr[0] === key) {
                return cookiearr[1]
            }
        }
        return null
    },
    removeCookie(key) {
        this.setCookie(key, null, -1)
    }
}