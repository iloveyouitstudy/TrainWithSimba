;
(($) => {
    $.extend({
        index: {}
    })

    $.extend($.index, {
        init() {
            this.loadList()
            this.bindEvent()
        },
        bindEvent() {
            $("#view").on("click", "button", function () {
                $(this).parents("li").remove()
            })
        },
        loadList() {
            layui.use(["laytpl", 'jquery'], () => {
                var laytpl = layui.laytpl
                var $ = layui.jquery

                var data = {
                    "userlist": [{
                        "name": "Simba",
                        "age": 20
                    }, {
                        "name": "zoro",
                        "age": 20
                    }]
                }

                laytpl($("#demo").html()).render(data, function (str) {
                    $('#view').html(str)
                });
            })
        }
    })

})(jQuery);