(function () {

	console.time("扩展jq耗时")

	$.fn.extend({
		mousescroll: function (callback) {
			var self = this[0]
			if (self.onmousewheel === undefined) {
				self.addEventListener("DOMMouseScroll", function (e) {
					//console.log( e.detail === 3? "下" : "上")
					callback(e.detail > 0)
				});
			} else {
				self.onmousewheel = function (e) {
					//console.log(e.wheelDelta === -120 ? "下" : "上")
					callback(e.wheelDelta < 0)
				}
			}
		}
	});

	console.timeEnd("扩展jq耗时")

})()