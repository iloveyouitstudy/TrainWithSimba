(function () {

	console.time("扩展jq耗时")

	$.extend({
		progressBar: function (color) {
			const str = `<div id="simba-progressBar" style="z-index:9999;height: 5px;background: ${color || "blue"};width:0;position: fixed;top: 0px;">
		</div>`;

			$("body").append(str);

			$(document).scroll(function () {
				var percent = $(document).scrollTop() / ($(document).height() - $(window).height());
				$("#simba-progressBar").css("width", (percent * 100).toFixed(2) + "%");
			})
		}
	});

	console.timeEnd("扩展jq耗时")

})()