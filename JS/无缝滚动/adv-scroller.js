;
(($) => {
    //扩展scrollerMove
    $.fn.extend({
        scrollerMove(speed) {
            //克隆一份一样的
            var clone = $(this).children().clone()
                        .css("margin-left", "-20px")
            $(this).append(clone)
            //移动的距离
            var juli = 0
            //定时器
            const createTimer = () => {
                return setInterval(() => {

                    juli--;
                    //向左移动
                    $(this).find(".box").css("transform", `translateX(${juli}px)`)
                    //当前面的已经出画面了
                    if (-juli - Math.floor($(".box").width()) >= 30) {
                        //前面的瞬间到后面去
                        $(this).find(".box").first().css("transform", `translateX(${-juli}px)`)
                        //距离重置 
                        juli = -30
                    }

                }, speed || 5);
            }
            //初始，开始动
            let timer = createTimer()
            // 鼠标悬浮停下，离开继续
            $(this).hover(() => {
                clearInterval(timer)
            }, () => {
                timer = createTimer()
            })
        }
    })

})(jQuery)

