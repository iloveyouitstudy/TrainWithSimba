 var cityList = [{
     id: 0,
     name: "全部"
 }, {
     id: 1,
     name: "北京"
 }, {
     id: 2,
     name: "南京"
 }, {
     id: 3,
     name: "上海"
 }]
 var typeList = [{
     id: 0,
     name: "全部"
 }, {
     id: 1,
     name: "演唱会"
 }, {
     id: 2,
     name: "戏剧"
 }, {
     id: 3,
     name: "体育比赛"
 }]
 var showList = []
 var Show = function (artist, id, name, img, city, cityid, address, time, price, state, typeid) {
     this.id = id
     this.name = name
     this.img = img
     this.city = city
     this.cityid = cityid
     this.address = address
     this.time = time
     this.price = price
     this.state = state
     this.typeid = typeid
     this.artist = artist
 }

 var show1 = new Show("理查德•克莱德曼", 1, "浪漫辉煌—理查德•克莱德曼2020北京新年音乐会", "https://img.alicdn.com/bao/uploaded/i1/2251059038/O1CN01e5kztA2GdSAP9I3Su_!!0-item_pic.jpg_q60.jpg_.webp",
     "北京", 1, "人民大会堂", "2019-12-25 19:30", [380, 680, 980, 1280], 0, 1)
 showList.push(show1)

 var show2 = new Show("仓木麻衣", 2, "2019仓木麻衣“20周年巡演计划”巡回演唱会-北京站", "https://img.alicdn.com/bao/uploaded/i4/2251059038/O1CN016ZeIfj2GdSAfwsyRC_!!0-item_pic.jpg_q60.jpg_.webp",
     "北京", 1, "北京展览馆剧场", "2019-11-25 19:30", [380, 680, 980], 0, 1)
 showList.push(show2)

 var show3 = new Show("马良", 3, "“往后余生 但愿有你” 马良2019巡演 南京站", "https://img.alicdn.com/bao/uploaded/i1/2251059038/O1CN01MMk2pj2GdSAU14PVs_!!0-item_pic.jpg_q60.jpg_.webp",
     "南京", 2, "欧拉艺术空间", "2019-10-25 19:30", [120], 2, 1)
 showList.push(show3)

 var show4 = new Show("黄子韬", 4, " 黄子韬2019 IS BLUE演唱会", "https://pimg.dmcdn.cn/perform/project/1768/176844_n.jpg?_t=1555038459571&x-oss-process=image/quality,q_60/format,webp",
     "上海", 3, "梅赛德斯-奔驰文化中心", "2019-6-25 19:30", [380, 680, 980, 1280], 1, 1)
 showList.push(show4)

 var show5 = new Show("南京大学艺术硕士剧团", 6, " 喜剧《蒋公的面子》", "https://pimg.dmcdn.cn/perform/project/1775/177596_n.jpg?_t=1557903795201&x-oss-process=image/quality,q_60/format,webp",
     "南京", 2, "江南剧院", "2019.06.07 19:30", [380, 680, 980, 1280], 0, 2)
 showList.push(show5)

 var show6 = new Show("中国男篮", 7, "2019 年中澳国际男篮对抗赛", "https://img.alicdn.com/bao/uploaded/i1/2251059038/O1CN01jNGUHG2GdSAcIpyIV_!!0-item_pic.jpg_q60.jpg_.webp",
     "南京", 2, "江南剧院", "2019.08.07 19:30", [380, 680], 0, 3)
 showList.push(show6)