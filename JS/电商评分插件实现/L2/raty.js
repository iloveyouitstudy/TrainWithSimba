;
(() => {
    $.fn.extend({
        raty(options) {

            var settings = {
                color: "gold",
                count: 5,
                score: 0,
                choose() {}
            }

            $.extend(settings, options)

            settings.container = "#" + this.attr("id")


            for (let i = 0; i < settings.count; i++) {
                var star = $(`<i class="fa star" style="color:${settings.color};cursor:pointer" aria-hidden="true"></i>`)
                if (i < settings.score) star.addClass("fa-star")
                else star.addClass("fa-star-o")
                $(settings.container).append(star)
            }

            $(settings.container + " .star").mouseenter(function () {
                setStar($(this).index() + 1)
            })

            $(settings.container + " .star").mouseleave(function () {
                setStar(settings.score)
            })

            $(settings.container + " .star").click(function () {
                settings.score = $(this).index() + 1
                settings.choose(settings.score)
            })

            function setStar(val) {
                $(settings.container + " .star").each(function (i) {
                    if (i < val) $(this).addClass("fa-star").removeClass("fa-star-o")
                    else $(this).removeClass("fa-star").addClass("fa-star-o")
                })
            }
        }
    })

})();