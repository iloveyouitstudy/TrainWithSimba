;
(() => {
    $(() => {
        $(window).click(function (e) {
            // console.log($(e.target));
            var target = $(e.target)
            if (target.is(".search_in")) $(".search_in").addClass("active")
            else $(".search_in").removeClass("active")
        })

        // $(".search_in").focus(function (e) {
        //     $(this).addClass("active")
        // }).blur(function () {
        //     $(this).removeClass("active")
        // })

        var loadData = () => {
            var keywords = $(".search_in").val().trim()
            var typeid = $(".typelist li.item.active").data("id")
            var courseid = $(".subjectList li.item.active").data("id")
            //动画出现
            $(".layer").show()
            //ajax，延迟一秒出现
            setTimeout(() => {
                // 获取数据
                $.get("data/data.json", data => {

                    if (typeid) data = data.filter(item => item.type === typeid)
                    if (courseid) data = data.filter(item => item.course === courseid)
                    if (keywords) {
                        data = data.filter(item => item.title.includes(keywords))
                        var reg = new RegExp(keywords, "g")
                        data.forEach(item => {
                            item.title = item.title.limit().replace(reg, `<strong style="color:red;">${keywords}</strong>`)
                        })
                    }
                    // 动画消失
                    $(".layer").hide()
                    $(".contents").html(template('temp2', data))
                })
            }, 500);
        }


        //限制文本溢出
        String.prototype.limit = function (count = 80) {
            if (this.length <= count) return this
            return this.substring(0, count) + "..."
        }

        $.get('data/type.json', arr => {
            $(".typelist").append(template('temp1', arr))
        })

        $.get('data/course.json', data => {
            $(".subjectList").append(template('temp1', data))
        })

        $(".menu").on("click", "li.item", function () {
            // console.log($(this).data("id"))//attr("data-id")
            $(this).addClass("active").siblings(".active").removeClass("active")
            loadData()
        })


        $(".btnsearch").click(function () {
            loadData()
        })

        var tagsList = []
        $.get('data/tags.json', data => {
            tagsList = data
            loadData()
        })

        template.defaults.imports.renderTags = function (tagid) {
            return tagsList.find(tag => tag.id === tagid).name
        }

    })
})();