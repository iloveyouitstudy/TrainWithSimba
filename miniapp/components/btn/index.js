// components/btn/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tip: {
      type: String,
      value: "请输入"
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    keywords: ""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    search() {
      //触发自定义事件
      this.triggerEvent("search", this.data.keywords)
    },
    setkeywords(e) {
      this.data.keywords = e.detail.value
    }
  }
})