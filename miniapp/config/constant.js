const BASE_URL = "https://easy-mock.com/mock/5d0899dec7eee8238592ae17/xcx"

const CACHE_KEY = {
  SHOP_CART: "shopcart"
}


module.exports = {
  BASE_URL,
  CACHE_KEY
}