// pages/calc/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodslists: [],
    total:0,
    myaddress1:"请选择地址",
    myaddress2:""
  },
  calcTotal() {
    this.data.total = 0
    this.data.goodslists.forEach(r => {
        this.data.total += r.count * r.guige.price
    })

    this.setData({
      total: this.data.total
    })
  },
  chooseaddress() {
    wx.chooseAddress({
      success:res=> {
        this.setData({
          myaddress1: `${res.userName}-${res.telNumber}`,
          myaddress2: `${res.provinceName}${res.cityName}${res.countyName}-${res.detailInfo}`
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    wx.request({
      url: 'http://localhost:9999/getGoodsByIdsandggids',
      data: {
        ids: JSON.parse(options.ids)
      },
      success: res => {
        console.log(res.data)

        this.setData({
          goodslists: res.data
        })

        this.calcTotal()

      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})