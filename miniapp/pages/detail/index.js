var $ = require('../../utils/ajax.js')
var constant = require('../../config/constant.js')
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    count: 1,
    goodsid: -1,
    activeType: "sp",
    goodsInfo: {},
    city: "南京",
    isshowlayer: false,
    guigelist: [],
    currentGG: {}
  },
  jian() {
    if (--this.data.count < 1) this.data.count = 1
    this.setData({
      count: this.data.count
    })
  },
  jia() {
    this.setData({
      count: ++this.data.count
    })
  },
  addCart() {

    var {
      count,
      currentGG,
      goodsid
    } = this.data

    console.log(app.globalData.userInfo.openId)

    //后台保存到购物车
    //同一个商品ID，count累加  
    //不同ID，新增记录
    wx.request({
      url: 'http://localhost:9999/addItemToShopCart',
      data: {
        count,
        guigeid: currentGG.id,
        goodsid,
        userid: app.globalData.userInfo.openId
      },
      success: res => {
        if (res.data.success) {
          wx.showToast({
            title: '加入购物车成功'
          })
        }
      }

    })
    // wx.setStorage({
    //   key: constant.CACHE_KEY.SHOP_CART,
    //   data: JSON.stringify(obj),
    //   success() {
    //     wx.showToast({
    //       title: '加入成功',
    //     })
    //   }
    // })

  },
  showlayer() {
    this.setData({
      isshowlayer: true
    })
  },
  closelayer() {
    this.setData({
      isshowlayer: false
    })
  },
  previewImg(e) {
    wx.previewImage({
      urls: [e.currentTarget.dataset.img],
    })
  },

  changtype(e) {
    this.setData({
      activeType: e.currentTarget.dataset.type
    })
  },

  changeLocation() {
    wx.chooseLocation({
      success: res => {
        $.getCityNameBD(res.longitude, res.latitude).then(res => {
          console.log(res)
          this.setData({
            city: res.province + res.city + res.district
          })
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    //访问后台接口，给邀请人返现 
    console.log("邀请人的唯一ID" + options.uid)


    wx.getLocation({
      type: 'gcj02',
      success: res => {
        $.getCityNameBD(res.longitude, res.latitude).then(res => {
          this.setData({
            city: res.province + res.city + res.district
          })
        })
      }
    })

    this.setData({
      goodsid: options.id
    })

    this.getDetail().then(title => {
      wx.setNavigationBarTitle({
        title
      })
    })

    this.getGGList()
  },

  setguige(e) {

    var obj = this.data.guigelist.find(r => r.id === e.currentTarget.dataset.id)

    this.setData({
      currentGG: obj
    })
  },

  getGGList() {
    wx.request({
      url: "http://localhost:9999/guigelist",
      data: {
        goodsid: this.data.goodsid
      },
      success: res => {
        this.setData({
          guigelist: res.data,
          currentGG: res.data[0]
        })
      }
    })
  },

  getDetail() {
    return $.ajax("/detail", {
      id: this.data.goodsid
    }).then(res => {
      this.setData({
        goodsInfo: res.data.goodsInfo
      })
      return res.data.goodsInfo.title
    })
  },
  onShareAppMessage(args) {

    return {
      title: "给你分享个好玩的东西",
      path: '/pages/detail/index?uid=' + app.globalData.userInfo.openId,
      imageUrl: this.data.goodsInfo.img[0]
    }
  }
})