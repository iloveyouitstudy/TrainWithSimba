var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeIndex: 0,
    tabTitleList: [
      "新品", "热销", "附近"
    ],

    advList: [{
        img: "http://p1.music.126.net/rA74LHVEvQd5T_6Zt71ayg==/109951164152955579.jpg",
        id: 10001
      },
      {
        img: "http://p1.music.126.net/cPt5nnlPUmjCaaBEzEcsDA==/109951164153165143.jpg",
        id: 12011
      },
      {
        img: "http://p1.music.126.net/8OqC4mQARCxrntlQ96i-lA==/109951164150006237.jpg",
        id: 30113
      }
    ],
    isShowWelcomeLayer: false
  },
  changeTab(e) {
    var index = e.currentTarget.dataset.index
    this.setData({
      activeIndex: index
    })
  },
  tabChange(e) {
    this.setData({
      activeIndex: e.detail.current
    })
  },
  close_layer() {
    this.setData({
      isShowWelcomeLayer: false
    })
  },
  onLoad() {
    // //当用户信息请求到之后执行
    // app.onUserInfoReady = () => {
    //   console.log(app.globalData.userInfo)
    // }
  },
  gofind() {
    wx.reLaunch({
      url: '../user/index',
    })
  }
})