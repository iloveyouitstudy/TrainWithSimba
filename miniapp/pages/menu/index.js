var $ = require('../../utils/ajax.js')
Page({
  data: {
    activeId: -1,
    keywords: "",
    typelist: [],
    goodslist: []
  },
  onLoad() {
    this.initLeftMenu()
    this.initGoodsList()
  },
  initLeftMenu() {
    $.ajax("/leftmenu").then(res => {
      this.setData({
        typelist: res.data.typelist,
        activeId: res.data.typelist[0].id
      })
    })
  },
  initGoodsList() {
    $.ajax("/submenulist", {
      typeid: this.data.activeId,
      keywords: this.data.keywords
    }).then(res => {
      this.setData({
        goodslist: res.data.submenulist
      })
    })
  },
  selectType(e) {
    this.setData({
      activeId: e.currentTarget.dataset.id
    })

    this.initGoodsList()
  },
  goDetail(e) {
    wx.navigateTo({
      url: `../search/index?id=${e.currentTarget.dataset.id}&title=${e.currentTarget.dataset.title}&bigid=${this.data.activeId}`
    })
  },
  inputKw(e) {
    this.data.keywords = e.detail.value
  },
  search() {
    this.initGoodsList()
  },
  scanCode() {
    wx.scanCode({
      success(res) {
        wx.navigateTo({
          url: '../detail/index?id=' + res.result
        })
      }
    })
  }
})