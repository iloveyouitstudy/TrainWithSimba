var _ = require('../../utils/simba.js')

Page({
  onReady: function(e) {
    // 使用 wx.createAudioContext 获取 audio 上下文 context
    this.audioCtx = wx.createAudioContext('myAudio')
  },
  data: {
    poster: 'http://y.gtimg.cn/music/photo_new/T002R300x300M000003rsKF44GyaSk.jpg?max_age=2592000',
    name: '成都',
    isPlaying: false,
    songtextlist: [],
    progress: 0,
    duration: "00:00",
    currentTime: "00:00",
    activeIndex: 0,
    trans: 200,
    src: 'http://58.216.6.145/amobile.music.tc.qq.com/C4000015y2qr1WX3t3.m4a?guid=8413245056&vkey=37572FB06C550D6CF3920752F584992D396BFA38DC615C5E782C29B00151708D952D844DE29995F4697DDA09192A14760AFF0442349E6F51&uin=4223&fromtag=66'
  },

  timeupdate(e) {

    var t1 = _.formatMs2Obj(e.detail.currentTime)
    var t2 = _.formatMs2Obj(e.detail.duration)

    this.setData({
      currentTime: t1.m + ":" + t1.s,
      duration: t2.m + ":" + t2.s,
      progress: (e.detail.currentTime * 100 / e.detail.duration).toFixed(4)
    })

    for (var i = 0; i < this.data.songtextlist.length; i++) {
      if (this.data.songtextlist[i].time === this.data.currentTime) {
        if (this.data.activeIndex !== i && this.data.songtextlist[i].text) {
          this.setData({
            activeIndex: i,
            trans: this.data.trans - 60
          })
          break
        }
      }
    }


  },

  audioPlay: function() {

    if (this.data.isPlaying) {
      this.audioCtx.pause()
    } else {
      this.audioCtx.play()
    }

    this.setData({
      isPlaying: !this.data.isPlaying
    })

  },
  onLoad() {

    var str = `
[00:00]玫瑰;
[00:00];
[00:01]词曲：贰佰;
[00:02]演唱：贰佰;
[00:04];
[00:28]你说你想在海边买一所房子;
[00:33];
[00:34]和你可爱的松狮一起住在那里;
[00:39];
[00:41]你会当一个心情杂货铺的老板娘;
[00:46];
[00:48]随着心情卖着自己喜欢的东西;
[00:53];
[00:55]生活越来越压抑;
[00:58];
[00:59]你变得越来越不像自己;
[01:02];
[01:02]一个人站在悲催的风里;
[01:06];
[01:09]玫瑰你在哪里;
[01:11];
[01:12]你说你爱过的人都已经离去;
[01:15];
[01:16]不要欺骗自己;
[01:18];
[01:19]你只是隐藏的比较深而已;
[01:22]玫瑰你在哪里;
[01:25];
[01:26]你总是喜欢抓不住的东西;
[01:29]请你不要哭泣;
[01:32];
[01:32]我们都只剩下一堆;
[01:34]用青春编织成的回忆;
[02:13]转眼两年时间已过去;
[02:17];
[02:20]该忘记的你有没有忘记;
[02:24];
[02:26]你说你最近爱上了旅行;
[02:30];
[02:33]我知道你也只是想逃避
    `

    var arr = str.split(';').map(r => {
      var obj = {}
      var arr = r.trim().substr(1).split(']')
      if (arr[0]) {
        obj.time = arr[0]
        obj.text = arr[1]
        return obj
      }
    })

    this.setData({
      songtextlist: arr
    })
  },

  audio14: function() {
    this.audioCtx.seek(14)
  },
  audioStart: function() {
    this.audioCtx.seek(0)
  },
  onLoad(){
    wx.request({
      url: 'http://localhost:5000/test',
      success:res=>{
        console.log(res.data)
      }
    })
  }
})