var $ = require('../../utils/ajax.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menulist: [],
    goodslist: [],
    id: 0,
    pageIndex: 1,
    pageSize: 6
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: options.title
    })

    this.setData({
      id: parseInt(options.id)
    })

    this.getSubMenus(options.bigid)
    this.getGoodslist()
  },
  getSubMenus(typeid) {
    $.ajax("/submenulist", {
      typeid
    }).then(res => {
      this.setData({
        menulist: res.data.submenulist
      })
    })
  },
  getGoodslist() {
    $.ajax("/goodslist", {
      submenuid: this.data.id,
      pageIndex: this.data.pageIndex,
      pageSize: this.data.pageSize
    }).then(res => {

      if (!res.data.goodslist.length) {
        wx.showToast({
          title: '没有更多了',
          icon: "none"
        })
        return false
      }

      var result = res.data.goodslist

      if (this.data.pageIndex > 1) {
        result = this.data.goodslist.concat(res.data.goodslist)
      }

      this.setData({
        goodslist: result
      })
    })
  },
  goDetail(e) {
    wx.navigateTo({
      url: '../detail/index?id=' + e.currentTarget.dataset.id
    })
  },
  selecttype(e) {
    this.setData({
      id: e.currentTarget.dataset.id
    })

    this.getGoodslist()
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    console.log("下拉了")
    setTimeout(() => {
      wx.stopPullDownRefresh()
      wx.hideNavigationBarLoading()
      this.data.pageIndex = 1
      this.getGoodslist()
    }, 1000)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    console.log("上拉触底了")
    this.data.pageIndex++
      this.getGoodslist()
  },

})