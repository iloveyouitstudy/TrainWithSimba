var app = getApp()

Page({
  data: {
    faceimg: '',
    nickname: ""
  },
  callme(){
    wx.makePhoneCall({
      phoneNumber: '18888889999' 
    })
  },
  onLoad() {
    if (app.globalData.userInfo) {
      this.setData({
        faceimg: app.globalData.userInfo.avatarUrl,
        nickname: app.globalData.userInfo.nickName
      })
    } else {
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            faceimg: app.globalData.userInfo.avatarUrl,
            nickname: app.globalData.userInfo.nickName
          })
        }
      })
    }
  },
  changeface() {
    wx.chooseImage({
      success: res => {

        this.setData({
          faceimg: res.tempFilePaths[0]
        })

        wx.uploadFile({
          url: 'http://localhost:9999/upload',
          filePath: res.tempFilePaths[0],
          name: 'face',
          formData: {
            'user': 'test'
          },
          success: res => {
            console.log(res)
          }
        })

        //调用后台接口，修改数据库，修改头像地址
        // wx.request({
        //   url: '/uploadface',
        // })


      },
    })
  }
})