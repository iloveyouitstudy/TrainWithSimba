var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: "",
    tel: "",
    gender: 1,
    roletype: 0,
    userid: -1,
    rolelist: [{
      type: 0,
      text: "消费者"
    }, {
      type: 1,
      text: "家装设计师"
    }, {
      type: 2,
      text: "家居设计师"
    }]
  },
  onLoad() {
    app.onOpenIdReady = res => {
      this.data.userid = res
    }
  },
  setgender(e) {
    this.setData({
      gender: parseInt(e.currentTarget.dataset.gender)
    })
  },
  setrole(e) {
    this.setData({
      roletype: parseInt(e.currentTarget.dataset.role)
    })
  },
  submit() {
    console.log(this.data)
    // wx.request({
    //   url: '/saveUserInfo',
    //   data: this.data
    // })
    wx.navigateBack()
  },
  setname(e) {
    this.data.name = e.detail.value
  },
  settel(e) {
    this.data.tel = e.detail.value
  }
})