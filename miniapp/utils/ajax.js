let constant = require('../config/constant.js')
let _ = require('../utils/simba.js')

var ajax = (url, data, method = "get") => {
  wx.showLoading({
    title: '加载中',
  })
  console.log(new Date().getFormatText() + "请求了" + url, "参数是" + JSON.stringify(data))
  return new Promise((success, f) => {
    wx.request({
      method,
      data,
      url: constant.BASE_URL + url,
      success,
      complete() {
        wx.hideLoading()
      }
    })
  })
}

var getCityName = (lo, la) => {
  return new Promise((s, f) => {
    wx.request({
      url: 'https://apis.map.qq.com/ws/geocoder/v1',
      data: {
        key: 'ADUBZ-WUPY4-PDPUT-XXLDW-VB7FH-ZJFKJ',
        location: (la + "," + lo)
      },
      success: res => {
        s(res.data.result.address_component)
      }
    })
  })
}

var getCityNameBD = (lo, la) => {
  return new Promise((s, f) => {
    wx.request({
      url: 'http://api.map.baidu.com/reverse_geocoding/v3',
      data: {
        ak:"bMwe5UMDLWVcmhaspEHB2HromPmARXvB",
        output:"json",
        coordtype:"wgs84ll",
        location: (la + "," + lo)
      },
      success: res => {
        s(res.data.result.addressComponent)
      }
    })
  })
}

module.exports = {
  ajax,
  getCityName,
  getCityNameBD
}