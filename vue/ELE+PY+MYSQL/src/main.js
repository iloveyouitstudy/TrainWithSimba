import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false
import axios from 'axios'
Vue.prototype.$http = axios
import elementui from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(elementui)

import mixin from './util/simba.mixin.js'

new Vue({
  render: h => h(App),
}).$mount('#app')
