import Vue from 'vue'

Vue.mixin({
    methods: {
        $msg(message, type) {
            if (this.$message) {
                this.$message({
                    message,
                    type
                });
            }
        },
        $fail(message) {
            this.$msg(message, "error")
        },
        $success(message) {
            this.$msg(message, "success")
        }
    }
})