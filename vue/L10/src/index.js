import Vue from 'vue'
import App from './App.vue'
import router from './routes/routes'
import 'font-awesome/css/font-awesome.min.css'
new Vue({
    el: "#app",
    router: router,
    render: h => h(App)
})