import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Index from '../views/index.vue'
import User from '../views/User.vue'
import Article from '../views/Article.vue'

const router = new VueRouter({
    routes: [{
        path: "/",
        component: Index,
        meta: {
            title: "主页"
        }
    }, {
        path: "/user",
        component: User,
        meta: {
            title: "用户"
        }
    }, {
        path: "/article",
        component: Article,
        meta: {
            title: "文章"
        }
    }]
})

export default router

router.beforeEach((to, from, next) => {
    if (to.meta && to.meta.title) {
        document.title = to.meta.title
    }
    next()
})