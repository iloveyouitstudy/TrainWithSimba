let PATH = require('path')
let CSS = require('extract-text-webpack-plugin')
let HTML = require('html-webpack-plugin')
let VUE = require('vue-loader/lib/plugin')
let CLEAN = require('clean-webpack-plugin')

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "js/main.js",
        path: PATH.resolve(__dirname, 'dist')
    },
    mode: 'production',
    module: {
        rules: [{
                test: /\.js/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css/,
                loader: CSS.extract({
                    use: "css-loader",
                    fallback: 'style-loader'
                })
            }, {
                test: /\.(png|jpg|jpeg|gif|bmp|ico|svg)/,
                loader: "url-loader?limit=2048&name=img/[name].[ext]"
            },
            {
                test: /\.(eot|woff|ttf|woff2)/,
                loader: "url-loader?name=font/[name].[ext]"
            },
            {
                test: /\.vue/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        new CLEAN(),
        new HTML({
            template: "./src/template.html"
        }),
        new CSS("main.css"),
        new VUE()
    ]
}