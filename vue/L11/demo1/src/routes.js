import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [{
        path: '/',
        component: () => import("@/views/Index.vue")
    }, {
        path: '/User',
        component: () => import("@/views/User.vue")
    }, {
        path: '/Order',
        component: () => import("@/views/Order.vue")
    }]
})

export default router