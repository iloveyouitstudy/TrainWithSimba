import Vue from 'vue'
import App from './App.vue'
import IView from 'iview'
import 'iview/dist/styles/iview.css'
Vue.use(IView);
import router from './routes'
import Axios from 'axios'
Vue.prototype.$http = Axios

Vue.config.productionTip = false
Vue.config.devtools = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')