import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [{
        path: '/',
        component: () => import("@/views/Index.vue")
    }, {
        path: '/classlist',
        component: () => import("@/views/ClassList.vue")
    }, {
        path: '/studentlist',
        component: () => import("@/views/StudentList.vue")
    }, {
        path: '/teacherlist',
        component: () => import("@/views/TeacherList.vue")
    }]
})

export default router