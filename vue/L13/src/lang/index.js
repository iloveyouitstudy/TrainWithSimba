import Vue from 'vue'
import VueI18n from 'vue-i18n'
import cn from './cn'
import en from './en'
Vue.use(VueI18n)
import Config from '@/config'

export default new VueI18n({
    locale: Config.lang,
    messages: {
        cn,
        en
    }
})