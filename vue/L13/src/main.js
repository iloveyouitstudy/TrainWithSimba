import Vue from 'vue'
import App from './App.vue'
import IView from 'iview'
import 'iview/dist/styles/iview.css'
Vue.use(IView);
import router from './routes'
import axios from './lib/request'
Vue.prototype.$http = axios
import i18n from '@/lang'

Vue.config.productionTip = false
Vue.config.devtools = false

new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')