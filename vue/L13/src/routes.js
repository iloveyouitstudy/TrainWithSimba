import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Config from '@/config'

var routes = []

Config.menuList.forEach(r => {
    r.sublist.forEach(rr => {
        routes.push({
            path: rr.path,
            meta: {
                title: r.label + "-" + rr.label
            },
            component: () => import("@/views/" + rr.component)
        })
    })
});


const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta && to.meta.title)
        document.title = to.meta.title
    next()
})

export default router