import Axios from '@/lib/request'
import {
    APIURI
} from '@/config'
export default {
    getClassList() {
        return Axios.get(APIURI.USER.STUDENTLIST)
    }
}