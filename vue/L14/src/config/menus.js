export const menuList = [{
        label: "学生管理",
        icon: "ios-paper",
        sublist: [{
                path: "/classlist",
                label: "班级列表",
                active: true,
                component: 'ClassList.vue'
            },
            {
                path: "/studentlist",
                label: "学生列表",
                component: 'StudentList.vue'
            },
            {
                path: "/teacherlist",
                label: "教员列表",
                component: 'TeacherList.vue'
            }
        ]
    },
    {
        label: "用户管理",
        icon: "ios-people",
        sublist: [{
                path: "/aaa",
                label: "用户列表",
                component: 'TeacherList.vue'
            }, {
                path: "/bbb",
                label: "测试一下",
                component: 'aaa.vue'
            },
            {
                path: "/xxxx",
                label: "用户列表2",
                component: 'TeacherList.vue'
            },
            {
                path: "/ffff",
                label: "用户列表3",
                component: 'TeacherList.vue'
            }
        ]
    },
    {
        label: "订单管理",
        icon: "ios-stats",
        sublist: [{
                path: "/ggg",
                label: "订单列表",
                component: 'TeacherList.vue'
            },
            {
                path: "/zzzz",
                label: "zzz列表",
                component: 'TeacherList.vue'
            },
            {
                path: "/gggg",
                label: "cccc列表",
                component: 'TeacherList.vue'
            }
        ]
    }
]