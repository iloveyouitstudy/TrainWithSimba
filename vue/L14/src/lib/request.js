import Axios from 'axios'
import {baseURL} from '@/config'
import iView from 'iview'
const axios = Axios.create({
    baseURL,
    timeout: 3000
});

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    iView.LoadingBar.start();
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    iView.LoadingBar.finish();
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


export default axios