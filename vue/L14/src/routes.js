import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import {
    menuList
} from '@/config'
import iView from 'iview';
import store from '@/store'


var routes = []

menuList.forEach(r => {
    r.sublist.forEach(rr => {
        routes.push({
            path: rr.path,
            meta: {
                title: r.label + "-" + rr.label
            },
            component: () => import("@/views/" + rr.component)
        })
    })
});


const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    if (to.meta && to.meta.title)
        document.title = to.meta.title
    next()
})

router.afterEach((to, from) => {
    iView.LoadingBar.finish();
    menuList.forEach((r, i) => {
        r.sublist.forEach((rr, j) => {
            if (rr.path === to.path) {
                store.commit("setActiveId", {
                    menuid: (i + 1).toString(),
                    labelid: i + 1 + "-" + (j + 1)
                });
            }
        });
    });

});


export default router