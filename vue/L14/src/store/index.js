import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        activeid: "1-1",
        username: "Simba",
        openmenusid: ["1"]
    },
    mutations: {
        setActiveId(state, {
            menuid,
            labelid
        }) {
            state.activeid = labelid
            state.openmenusid = [menuid]
        }
    }
})

export default store