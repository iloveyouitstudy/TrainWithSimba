import {
    fetch
} from '@/lib'
import {
    APIURI
} from '@/config'
export default {
    //登录请求
    login({
        nickname,
        password
    }) {
        return fetch.post(APIURI.ACCOUNT.LOGIN, {
            nickname,
            password
        })
    }
}