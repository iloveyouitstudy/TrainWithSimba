import user from './user'

import account from './account'
import Vue from 'vue'
const obj = {
    user,
    account,
    install(Vue, options) {
        Vue.prototype.$service = this
    }
}



export default obj