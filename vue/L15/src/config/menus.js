export default [{
        label: "登录",
        path: "/login",
        component: 'account/login.vue',
    }, {
        label: "学生管理",
        inMenu: true,
        icon: "ios-paper",
        path: "/student",
        component: 'App.vue',
        sublist: [{
                path: "classlist",
                inMenu: true,
                label: "班级列表",
                component: 'ClassList.vue'
            },
            {
                path: "studentlist",
                inMenu: true,
                label: "学生列表",
                component: 'StudentList.vue'
            },
            {
                path: "teacherlist",
                inMenu: true,
                label: "教员列表",
                component: 'TeacherList.vue'
            }
        ]
    },
    {
        label: "用户管理",
        inMenu: true,
        icon: "ios-people",
        path: "/user",
        component: 'App.vue',
        sublist: [{
                path: "aaa",
                inMenu: true,
                label: "用户列表",
                component: 'TeacherList.vue'
            }, {
                path: "bbb",
                inMenu: true,
                label: "测试一下",
                component: 'aaa.vue'
            },
            {
                path: "xxxx",
                inMenu: true,
                label: "用户列表2",
                component: 'TeacherList.vue'
            }
        ]
    },
    {
        label: "订单管理",
        inMenu: true,
        icon: "ios-stats",
        path: "/order",
        component: 'App.vue',
        sublist: [{
                path: "ggg",
                inMenu: true,
                label: "订单列表",
                component: 'TeacherList.vue'
            },
            {
                path: "zzzz",
                label: "zzz列表",
                inMenu: true,
                component: 'TeacherList.vue'
            },
            {
                path: "gggg",
                label: "cccc列表",
                inMenu: true,
                component: 'TeacherList.vue'
            }
        ]
    }
]