import Vuex from 'vuex'
import Vue from 'vue'
import {
    storage
} from '@/lib'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        activeid: "1-1",
        nickname: "",
        openmenusid: ["1"]
    },
    getters: {
        getNickName(state) {
            return state.nickname || storage.get("userinfo");
        }
    },
    mutations: {
        logout(state){
            state.nickname = ""
            storage.remove("userinfo");
        },
        setUserInfo(state, data) {
            storage.set("userinfo", data.nickname);
            state.nickname = data.nickname
        },
        setActiveId(state, {
            menuid,
            labelid
        }) {
            state.activeid = labelid
            state.openmenusid = [menuid]
        }
    }
})

export default store