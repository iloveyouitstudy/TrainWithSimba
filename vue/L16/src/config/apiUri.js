export const APIURI = {
    USER: {
        STUDENTLIST: "studentlist"
    },
    ACCOUNT: {
        LOGIN: "login"
    },
    SYSTEM: {
        COUNT: 'home/count',
        BZSJ: "/home/bzsj",
        BZLXSJ: "/home/bzlxsj"
    }
}