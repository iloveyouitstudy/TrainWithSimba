export const storage = {
    set(key, val) {
        sessionStorage.setItem(key, val);
    },
    get(key) {
        return sessionStorage.getItem(key);
    },
    remove(key) {
        sessionStorage.removeItem(key);
    }
}