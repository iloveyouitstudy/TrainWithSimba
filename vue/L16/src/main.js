import Vue from 'vue'
import App from './App.vue'
import IView from 'iview'
import 'iview/dist/styles/iview.css'
Vue.use(IView);
import router from './routes'
import Service from './api'
Vue.use(Service)
import i18n from '@/lang'
Vue.config.productionTip = false
Vue.config.devtools = false
import store from './store'
// import VueAwesomeSwiper from 'vue-awesome-swiper'
// import 'swiper/dist/css/swiper.css'
// Vue.use(VueAwesomeSwiper, /* { default global options } */)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  mounted() {
    // this.$Loading.config({
    //   color: 'green',
    //   height: 5
    // });
  },
}).$mount('#app')