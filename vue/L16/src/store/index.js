import Vuex from 'vuex'
import Vue from 'vue'
import user from './user'
import order from './order'
import system from './system'
Vue.use(Vuex)
const store = new Vuex.Store({
    modules: {
        user,
        system,
        order
    }
})

export default store