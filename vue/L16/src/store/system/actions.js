import Service from '@/api'
export default {
    getHomeCount() {
        return Service.system.getHomeCount()
    },
    getHomeBZSJ() {
        return Service.system.getHomeBZSJ()
    },
    getHomeBZLXSJ() {
        return Service.system.getHomeBZLXSJ()
    }
}