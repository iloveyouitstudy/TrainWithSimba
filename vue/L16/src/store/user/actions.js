import Service from '@/api'
export default {
    loadUsers() {
        return Service.user.getClassList()
    }
}