import {
    storage
} from '@/lib'
export default {
    logout(state) {
        state.nickname = ""
        storage.remove("userinfo");
    },
    setUserInfo(state, data) {
        storage.set("userinfo", data.nickname);
        state.nickname = data.nickname
    },
    setActiveId(state, {
        menuid,
        labelid
    }) {
        state.activeid = labelid
        state.openmenusid = [menuid]
    }
}