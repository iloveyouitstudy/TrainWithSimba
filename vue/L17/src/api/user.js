import {
    fetch
} from '@/utils'
import {
    URI
} from '@/config'

export default {
    GetUserInfo() {
        return fetch.get(URI.USER.GETUSERINFO)
    }
}