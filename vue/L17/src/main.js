import Vue from 'vue'
import App from './App.vue'
import Mint from 'mint-ui';
import 'mint-ui/lib/style.css'
Vue.use(Mint);
Vue.config.productionTip = false
import router from './router'
// Vue.prototype.$http = Axios
import store from '@/store'
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')