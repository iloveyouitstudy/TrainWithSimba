import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import Service from '@/api'
export default new Vuex.Store({
    state: {},
    getters: {},
    mutations: {
    },
    actions: {
        getUserInfo(state) {
            return Service.User.GetUserInfo()
        }
    },
    modules: {}
})