import Axios from 'axios'

const axios = Axios.create({
    baseURL: "http://47.92.50.43:3008/webapi/api"
})


class Fetch {
    constructor(context) {
        this.context = context
    }

    get(url, params) {
        return this.context.get(url, {
            params
        })
    }

    post(url, params) {
        return this.context.post(url, params)
    }
}

// function Fetch(context){
//     this.get = function(url){
//         return context.get(url)
//     }

//     this.post = function(url){
//         return context.post(url)
//     }
// }

export const fetch = new Fetch(axios)