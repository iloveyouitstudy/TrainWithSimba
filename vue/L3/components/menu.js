Vue.component("sb-menu",{
    props:["title","list","value"],
    template:`
            <ul class="menus">
                <li style="width:100px;text-align:right">{{title}}</li>
                <li class="item" v-bind:class="{active:item.id===value}" 
                v-for="item in list" @click="$emit('input',item.id)">
                    {{item.name}}
                </li>
            </ul>
    `
})