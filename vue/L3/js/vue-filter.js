Vue.filter("formatType", (val) => {
    switch (val) {
        case 0:
            return "当季"
        case 1:
            return "特价"
        case 2:
            return "海外"
        case 3:
            return "清仓"
    }
})


Vue.filter("formatGender", (val) => {
    switch (val) {
        case 0:
            return "男"
        case 1:
            return "女"
        default:
            return "未知"
    }
})