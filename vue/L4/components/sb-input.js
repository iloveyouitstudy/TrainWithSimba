Vue.component("sb-input", {
    props: ["placeholder", "value"],
    template: `
    <div style="border:1px solid #ccc;display: flex;height: 30px;">
        <input :placeholder="placeholder" type="text" 
        style="width:calc(100% - 30px) ;border:none;outline: none;
        text-indent: 10px" :value="value" 
        @input="$emit('input', $event.target.value)"/>
        <button @click="search" style="outline: none;cursor: pointer;
            border:none;width:30px;
            background:#fff url(img/bn_srh_1.png) no-repeat center center">
        </button>
    </div>
    `,
    methods: {
        search() {
            this.$emit("search", this.value)
        }
    }
})