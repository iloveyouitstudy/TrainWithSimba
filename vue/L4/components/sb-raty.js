Vue.component("sb-raty", {
    props: ["count", "value"],
    template: `
        <div>
            <i v-for="i in c" class="fa" 
            :class="i<=tempscore ?'fa-star':'fa-star-o'" 
            @mouseover="tempscore=i" 
            @mouseout="tempscore=s" 
            @click="s = i"></i>
        </div>
    `,
    watch: {
        s(val) {
            this.$emit("change", val)
            this.$emit("input", val)
        }
    },
    data() {
        return {
            s: this.value,
            c: this.count,
            tempscore: this.value
        }
    },
})