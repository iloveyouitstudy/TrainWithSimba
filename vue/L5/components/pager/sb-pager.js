Vue.component("sb-pager", {
    props: {
        pagecount: {
            type: Number,
            required: true,
            validator(val) {
                return val > 0
            }
        },
        value: {
            type: Number,
            default: 1,
            validator(val) {
                return val > 0
            }
        }
    },
    mounted() {
        if (this.value > this.pagecount) throw new Error("页码不能大于总页数")
    },
    template: `
        <ul class="sb-pager" style="margin:100px;">
            <li @click="pageIndex=1" :class="{disabled:pageIndex===1}">首页</li>
            <li @click="pageIndex--" :class="{disabled:pageIndex===1}">上页</li>
            <li v-for="i in pagecount" :class="{active:i===pageIndex}" @click="choose(i)">{{i}}</li>
            <li @click="pageIndex++" :class="{disabled:pageIndex===pagecount}">下页</li>
            <li @click="pageIndex=pagecount" :class="{disabled:pageIndex===pagecount}">末页</li>
        </ul>
    `,
    data() {
        return {
            pageIndex: this.value
        }
    },
    watch: {
        pageIndex(val) {
            if (val < 1) this.pageIndex = 1
            if (val > this.pagecount) this.pageIndex = this.pagecount
            this.$emit("input", this.pageIndex)
        }
    },
    methods: {
        choose(i) {
            this.pageIndex = i

        }
    },
})