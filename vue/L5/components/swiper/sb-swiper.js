Vue.component("sb-swiper", {
    props: {
        imglist: {
            required: true,
            type: Array
        },
        interval: {
            type: Number,
            default: 1000,
            validator(val) {
                return !isNaN(val) && val > 0
            }
        }
    },
    template: `
    <div class="sb-swiper-container">
        <ul class="sb-swiper">
            <li v-for="(item,i) in imglist" v-show="i===index">
                <a :href="item.href" target="_blank">
                    <img :src="item.img" :title="item.title" :alt="item.title">
                </a>
            </li>
        </ul>
        <ul class="sb-swiper-dots">
            <li :class="{active:(i-1)===index}" @mouseout="move" @mouseover="choose(i-1)" v-for="i in imglist.length"></li>
        </ul>
    </div>
    `,
    data() {
        return {
            index: 0,
            timer: -1,
        }
    },
    methods: {
        choose(i) {
            clearInterval(this.timer)
            this.index = i
        },
        move() {
            this.timer = setInterval(() => {
                this.index++
                if (this.index === this.imglist.length) this.index = 0
            }, this.interval)
        }
    },
    mounted() {
        this.move()
    },
})