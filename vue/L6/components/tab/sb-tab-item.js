Vue.component("sb-tab-item", {
    props: ["label"],
    template: `
        <div v-show="isshow">
            <slot></slot>
        </div>
    `,
    data() {
        return {
            isshow: false
        }
    },
    mounted() {
        this.$parent.labellist.push(this.label)
    },
})