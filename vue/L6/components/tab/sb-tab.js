// Vue.config.keyCodes = {
//     left: 37,
//     right: 39
// }


Vue.component("sb-tab", {
    template: `
        <div class="sb-tab" >
            <ul class="sb-tab-title">
                <li @click="index=i" :class="{active:i===index}" v-for="(item,i) in labellist">{{item}}</li>
            </ul>
            <div class="sb-tab-content">
                <slot></slot>
            </div>
        </div>
    `,
    data() {
        return {
            index: 0,
            labellist: []
        }
    },
    watch: {
        index(val) {
            if (this.index < 0) this.index = this.labellist.length - 1
            if (this.index === this.labellist.length) this.index = 0

            this.$children.forEach(item => {
                item.isshow = false
            });
            this.$children[this.index].isshow = true

            this.$emit("change", this.index)
        }
    },
    mounted() {
        this.$children[0].isshow = true
        // this.labellist = this.$children.map(item => {
        //     return item.label
        // });

        window.onkeydown = (e) => {
            if (e.keyCode === 39) {
                this.index++
            } else if (e.keyCode === 37) {
                this.index--
            }
        }
    },
    methods: {

    },
})