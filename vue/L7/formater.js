Vue.directive("time", {
    bind(el, binding) {
        var settime = function () {
            el.innerText = Formater.fmtTime(binding.value)
        }
        settime()

        el.__settime__ = settime
        el.__timer__ = setInterval(settime, 1000 * 60)
    },
    unbind(el) {
        el.__settime__ = null
        clearInterval(el.__timer__)
        delete el.__settime__
        delete el.__timer__
    }
})

Vue.filter("fmtTime", (val) => {
    return Formater.fmtTime(val)
})

const Formater = {
    buling(val) {
        return val < 10 ? ("0" + val) : val
    },
    fmtTime(val) {
        //过去时间
        var date = new Date(val)
        //当前时间
        var now = new Date()
        var cha = now - date
        if (cha < 60 * 1000) {
            return "刚刚"
        } else if (cha < 60 * 60 * 1000) {
            return Math.floor(cha / (60 * 1000)) + "分钟前"
        } else if (cha < 60 * 60 * 1000 * 24) {
            return Math.floor(cha / (60 * 60 * 1000)) + "小时前"
        } else {
            return date.getFullYear() + "-" +
                this.buling(date.getMonth() + 1) + "-" +
                this.buling(date.getDate()) + " " +
                this.buling(date.getHours()) + ":" +
                this.buling(date.getMinutes()) + ":" +
                this.buling(date.getSeconds())
        }
    }
}