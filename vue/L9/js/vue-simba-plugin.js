const vue_simba_plugin = (() => {
    const vue_simba_plugin = {}
    vue_simba_plugin.install = function (Vue, options) {
        //通用主机地址
        Vue.prototype.$host = options.host;
        //通用ajax方法
        Vue.prototype.$http = axios
        //通用Log
        Vue.prototype.$log = console.log
        //关闭提示
        Vue.config.productionTip = false
        Vue.config.devtools = false
        //提供自定义组件
        Vue.component("sb-input", {
            template: `<input/>`
        })
        //提供自定义指令
        Vue.directive("focus", {
            inserted(el) {
                el.focus()
            }
        })
        //提供通用KeyCode
        Vue.config.keyCodes = {
            simbaenter: 13,
            down: 84
        }
    }

    return vue_simba_plugin
})()