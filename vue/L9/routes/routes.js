const router = new VueRouter({
    routes: [{
        name: "msg",
        path: "/msg",
        component: view_xiaoxi,
        meta: {
            title: "消息列表"
        }
    }, {
        name: "fri",
        path: "/friends",
        component: view_friends,
        meta: {
            title: "好友列表"
        }
    }, {
        path: "/find",
        component: view_find,
        meta: {
            title: "发现"
        }
    }, {
        path: "/mine",
        component: view_mine,
        meta: {
            title: "个人中心",
            needLogin: true
        }
    }, {
        path: "/login",
        component: view_login,
        meta: {
            title: "登录"
        }
    }, {
        path: "/chat/:id",
        component: view_chat,
        props: true,
        meta: {
            title: "聊天"
        }
    }]
})

//导航守卫
router.beforeEach(function (to, from, next) {
    if (to.meta.needLogin) {
        console.log("需要登录")
        if (!localStorage.getItem("loginuser")) {
            router.push('/login')
            return false;
        }
    }
    document.title = to.meta.title
    next()
})