const view_mine = Vue.component("mine", {
    template: `<div>
    {{nickname}}
    </div>`,
    data() {
        return {
            nickname: localStorage.getItem('loginuser')
        }
    },
})