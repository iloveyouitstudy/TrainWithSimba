var _ = require('../../utils/simba.js')
var $ = require("../../utils/ajax.js")

Page({
  data: {
    nickname: "",
    userlist: [],
    img: ""
  },
  onLoad() {
    $.ajax("/user").then(res => {
      this.setData({
        userlist: res.data.userlist
      })
    })
  },
  add() {
    if (!this.data.nickname) {
      wx.showToast({
        title: '不能为空哦~',
        icon: 'none',
        duration: 1000
      })
      return false
    }
    this.data.userlist.push({
      id: 10005,
      name: this.data.nickname
    })

    this.setData({
      userlist: this.data.userlist,
      nickname: ''
    })
  },
  setnickname(e) {
    this.data.nickname = e.detail.value
  },
  del(e) {

    wx.showModal({
      title: '温馨提示',
      content: '确定删除吗？',
      success: res => {
        if (res.confirm) {
          var obj = this.data.userlist.find(item => item.id === e.currentTarget.dataset.id)
          this.data.userlist.remove(obj)
          this.setData({
            userlist: this.data.userlist
          })
        }
      }
    })


  },
  chooseface() {

    // wx.makePhoneCall({
    //   phoneNumber: '10086' //仅为示例，并非真实的电话号码
    // })

    wx.vibrateLong()

    // wx.chooseImage({
    //   success: res => {
    //     this.setData({
    //       img: res.tempFiles[0].path
    //     })
    //   },
    // })



  }
})