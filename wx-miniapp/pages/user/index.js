// pages/user/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickname: "",
    src: 'http://47.92.50.43:9876/di.mp3',

  },
  gg(e) {
    this.setData({
      nickname: e.detail.result
    })

    this.audioCtx.play()
    
  },
  onReady: function(e) {
    // 使用 wx.createAudioContext 获取 audio 上下文 context
    this.audioCtx = wx.createAudioContext('myAudio')

    this.audioCtx.play()
  },
})