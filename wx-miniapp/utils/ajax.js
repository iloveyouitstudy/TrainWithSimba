let constant = require('../config/constant.js')
let _ = require('../utils/simba.js')

var ajax = (url) => {
  wx.showLoading({
    title: '加载中',
  })
  console.log(new Date().getFormatText() + "请求了" + url)
  return new Promise((success, f) => {
    wx.request({
      url: constant.BASE_URL + url,
      success,
      complete(){
        wx.hideLoading()
      }
    })
  })
}

module.exports = {
  ajax
}